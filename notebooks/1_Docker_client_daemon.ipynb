{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Docker Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Objective**\n",
    "\n",
    "This notebook gives an overview of Docker and Docker architecture, and introduces basic Docker objects and client commands.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Docker and Docker architecture"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Docker**\n",
    "    \n",
    "Docker is an open source platform written in go language for developping, shipping and running applications. \n",
    "    \n",
    "It helps you to separate and manage the infrastructure of your application through use of containers. \n",
    "\n",
    "You have an isolated environment that contains everything necessary to run your application independent of what is installed on the host machine. \n",
    "    \n",
    "It is lightweight and easily portable.\n",
    "\n",
    "With Docker you can test and distribute your application in the form of containers.\n",
    "    \n",
    "Docker takes advantage of several existing Linux kernel features, especially **[namespaces](https://docs.docker.com/glossary/#namespace)** and **[cgroups](https://docs.docker.com/glossary/#cgroups)** to isolate the container processes.\n",
    "    \n",
    "<font color=gray>See more on [Demystifying Containers](https://medium.com/@saschagrunert/demystifying-containers-part-i-kernel-space-2c53d6979504).</font>\n",
    "    \n",
    "Docker basically is composed of \n",
    "\n",
    "- a command line program (Docker client),\n",
    "- a background process (Docker server) and\n",
    "- some remote services (Registry).\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Docker architecture**\n",
    "\n",
    "Docker uses a client-server architecture where client and server communicates through a REST API over Unix sockets or a network interface.\n",
    "\n",
    "<img src=\"../img/docker-architecture.webp\" float=\"left\" width=\"80%\" style=\"padding:10px;\">\n",
    "    \n",
    "**Docker client** is a command line tool that uses Docker REST API to communicate with the Docker daemon.\n",
    "    \n",
    "**Docker daemon** is a service that listens to the REST API requests and takes action depending on the request. It manages **Docker objects**; builds docker images, pulls images from registry, runs containers from images, etc. It can either run on the same machine with Docker client or on a remote host.\n",
    "\n",
    "**Docker registry** stores Docker images. Default is [Docker Hub](https://hub.docker.com/).\n",
    "    \n",
    "[*](../LICENSE_AL2)\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**Docker objects**: images, containers, networks, volumes, etc.\n",
    "\n",
    "**Docker plugins**: buildx, compose, etc.\n",
    "\n",
    "**Docker extensions**: We will not talk about extensions.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "<font color=gray>\n",
    "Docker daemon binds to a Unix socket which requires root privileges. To use Docker client as a non-root user, you can add your user to \"docker\" group.\n",
    "</font >   \n",
    "\n",
    "`$ sudo usermod -aG docker $USER`\n",
    "\n",
    "<font color=gray>\n",
    "Then logout and login back.\n",
    "</font >\n",
    "\n",
    "***\n",
    "\n",
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(255, 230, 230); border:1px solid transparent; border-radius: 6px; border-color: rgb(255, 230, 230); padding:10px; margin: 10px;\">\n",
    "\n",
    "**ATTENTION**\n",
    "    \n",
    "Adding users to \"docker\" group implicitly enables them to have root privileges on the host machine through use of containers.\n",
    "    \n",
    "<font color=gray>See more on [Docker-daemon attack surface](https://docs.docker.com/engine/security/#docker-daemon-attack-surface) </font>\n",
    "\n",
    "</div>\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**NOTE**\n",
    "    \n",
    "It is also possible to install Docker in [rootless mode](https://docs.docker.com/engine/security/rootless/) since version 20.10.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Docker CLI"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use Docker client commands throughout the training to communicate with docker daemon.\n",
    "\n",
    "Before all, we will check docker daemon (server) status."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(235, 235, 224); border:1px solid transparent; border-color: (235, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Run in your terminal** \n",
    "\n",
    "$ sudo systemctl status docker\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the server is active and running, we can start using Docker CLI to communicate with the server.\n",
    "\n",
    "We can display the list of available Docker CLI commands and their syntax either running `docker` with no parameter or running `docker --help`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**NOTE**\n",
    "    \n",
    "It is encouraged to use **Docker management commands** since Docker version 1.13. \n",
    "\n",
    "<font color=gray>See more on [CLI restructured](https://www.docker.com/blog/whats-new-in-docker-1-13/) and [CLI reference](https://docs.docker.com/engine/reference/commandline/docker/).</font >\n",
    "    \n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display version for Docker client and server:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will first check Docker version installed using option `--version`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker --version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show help on a Docker `management command`:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can list Docker client commands on a *logical object* (image, container, network, etc.) either running `docker <object>` with no parameters or running `docker <object> --help`:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display list of commands on **container** object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display list of commands on **image** object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display list of commands on **network** object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker network"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display list of commands on **volume** object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker volume"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display list of commands on **system** object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also list the possible commands on Docker plugins with management commands.\n",
    "Display list of commands on `buildx` client:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker buildx"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker compose --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Show help on a Docker `command` on logical objects:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display help on a Docker client command on a logical object running `docker <object> <command> --help`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**NOTE**\n",
    "\n",
    "<font color=gray>`docker container run` is equivalent of `docker run`. </font>\n",
    "    \n",
    "    \n",
    "<font color=gray>See more under [CLI restructured](https://www.docker.com/blog/whats-new-in-docker-1-13/) and [CLI reference](https://docs.docker.com/engine/reference/commandline/docker/).</font >\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Container and Image Commands"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will focus on container and image commands as they are the docker objects that we will interact mostly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run a Docker container"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "    \n",
    "A **container** is an isolated environment on the host machine that contains all necessary dependencies and configurations to run an application component. When a container is run, it becomes an isolated process.\n",
    "\n",
    "<div>\n",
    "    \n",
    "<font color=gray>More on images and containers in the notebook [2_Dockerfile_Images_Containers](./2_Dockerfile_Images_Containers.ipynb).</font>\n",
    "    \n",
    "</div>\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**NOTE**\n",
    "\n",
    "**Containers vs. virtual machines**\n",
    "\n",
    "A virtual machine has an entire operating system with its own kernel, hardware drivers, programs, etc.\n",
    "\n",
    "A container is an isolated process with all its files necessary to run it. A container uses the kernel of the host machine and shares it with other containers on the host machine.\n",
    "\n",
    "Using a VM to isolate a single application will be overuse of resources. \n",
    "\n",
    "In general VM's are used to run multiple containers.\n",
    "\n",
    "<img src=\"../img/containers_vs_vms.png\" float=\"left\" width=\"98%\" style=\"padding:10px;\">\n",
    "\n",
    "<font color=gray>More on containers vs virtual machines [Use containers to Build, Share and Run your applications](https://www.docker.com/resources/what-container/).</font>\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The command to run a Docker container `docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]` actually requires an **IMAGE**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "    \n",
    "A **Docker image** is a blueprint from which individual containers are created. It contains the application and all its dependencies that it needs to run correctly. \n",
    "\n",
    "The images are immutable.\n",
    "\n",
    "A **Docker container** in runnable instance created from a Docker image.\n",
    "\n",
    "A container creates a writable layer on top of the image.\n",
    "\n",
    "<div>\n",
    "    \n",
    "<font color=gray>More on images and containers in the notebook [2_Dockerfile_Images_Containers](./2_Dockerfile_Images_Containers.ipynb).</font>\n",
    "    \n",
    "</div>\n",
    "    \n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have two options to run a container from an image:\n",
    "\n",
    "- Use a pre-compiled Docker image\n",
    "- Build a Docker image from Dockerfile and use it"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run a container from a pre-compiled Docker image**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to list all available images on host machine by `docker image ls` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "[Docker Hub](https://hub.docker.com/) is the default Docker registry where we can search and download precompiled images.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From Docker Hub, we are going to download `hello-world` image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hide_output"
    ]
   },
   "outputs": [],
   "source": [
    "docker image pull hello-world"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=gray>You can check [hello-world image](https://hub.docker.com/_/hello-world/) on Docker Hub.</font >"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List local images:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that **hello-world** image is listed among the images on the host machine. \n",
    "\n",
    "Now we can run a container from that image using `docker container run` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run hello-world"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can list the running containers on the host machine using `docker container ls` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We do not see our container in the list. That is because without options, `docker container ls` command lists only active (running) containers. `-a` option displays all containers.\n",
    "\n",
    "The container run from `hello-world` image is started, run to display its message and then stopped. That's why we do not see the container among active containers.\n",
    "\n",
    "We can list all containers (active + inactive) using `-a` flag:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we see that from *IMAGE* **hello-world** a *CONTAINER* is created and exited."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**NOTE**\n",
    "\n",
    "Alternatively we could run:\n",
    "\n",
    "`docker container run hello-world`\n",
    "\n",
    "without pulling the image. This would at first download the image if it does not exist locally and then run a container from that image.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Build an image from Dockerfile and run**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides downloading an existing image from a Docker repository and running a container from that image, it is possible to create a Docker image on host machine from a Dockerfile."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Dockerfile**\n",
    "\n",
    "`Dockerfile` is a text file that contains all the necessary instructions to build a Docker image.\n",
    "\n",
    "<font color=gray>More on Dockerfiles in the notebooks [2_Dockerfile_Images_Containers](2_Dockerfile_Images_Containers.ipynb) and [3_More_on_Dockerfile](3_More_on_Dockerfile.ipynb).</font >\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Dockerfile](../examples/ex1/Dockerfile) under **../examples/ex1/** directory has the following contents:\n",
    "\n",
    "```Dockerfile\n",
    "FROM alpine:3.20\n",
    "\n",
    "CMD [\"echo\", \"This container is created from the image that you just built!!\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "This Dockerfile uses a base [alpine image](https://hub.docker.com/layers/library/alpine/3.20/images/sha256-029a752048e32e843bd6defe3841186fb8d19a28dae8ec287f433bb9d6d1ad85?context=explore) from [Docker Hub](https://hub.docker.com/).\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To build a Docker image from Dockerfile, `docker image build`, `docker buildx build` or simply `docker build` command is used. These are all aliases for the command to build a Docker image."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 235); border:1px solid transparent; border-color: rgb(224, 235, 235); padding:10px; margin: 10px;\">\n",
    "\n",
    "**[Buildx](https://github.com/docker/buildx)** is a Docker CLI plugin to build Docker images.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**NOTE**\n",
    "\n",
    "Previously `docker image build` or `docker build` commands were used to build an image. \n",
    "\n",
    "Starting from **Docker version 23.0**, Docker is using **[Buildx/Builkit](https://docs.docker.com/build/concepts/overview/)** for building images.  \n",
    "\n",
    "Currently `docker image build` and `docker build` commands are just wrappers for `docker buildx build` command. We will use this latter command throughout the training. \n",
    "\n",
    "\n",
    "<div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can display help on its usage with the command `docker buildx build --help`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker buildx build --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will build Docker image with tag **img_ex1:v1**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker buildx build -t img_ex1:v1 ../examples/ex1/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use `--tag` flag (alternatively `-t`) to tag an image.\n",
    "\n",
    "Let's see the created image in image list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will run a container from the image **img_ex1:v1** that we just built using [Dockerfile](../examples/ex1/Dockerfile) under **../examples/ex1/** directory :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run img_ex1:v1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we see that from *IMAGE* **img_ex1:v1** a second *CONTAINER* is created and exited.\n",
    "\n",
    "The names of the two containers are given randomly by Docker.\n",
    "\n",
    "<div>\n",
    "    \n",
    "<font color=gray>More on Dockerfiles in the notebooks [2_Dockerfile_Images_Containers](2_Dockerfile_Images_Containers.ipynb) and [3_More_on_Dockerfile](3_More_on_Dockerfile.ipynb).</font >\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run a named container**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively we can run a container by specifying its name. \n",
    "\n",
    "`--name` option is used to give a specific name to the container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --help"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --name cont_ex1 img_ex1:v1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List all containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have created a new container from image `img_ex1:v1` with name `cont_ex1`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run a container interactively**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run a container interactively, use `--interactive` flag (alternatively `-i`). \n",
    "\n",
    "To allocate a pseudo-TTY command line interpreter or shell that attaches stdin and stdout, use `--tty` flag (alternatively `-t`) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(235, 235, 224); border:1px solid transparent; border-color: (235, 235, 224); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Run in your terminal** \n",
    "    \n",
    "(To launch a terminal use menu File -> New -> Terminal)\n",
    "\n",
    "    \n",
    "`$ docker container run --interactive --tty --name cont_ex1_int1 alpine:3.20 ash`\n",
    "\n",
    "\n",
    "This will run a Docker container from Alpine image version 3.16 and connect to it via ash shell. \n",
    "\n",
    "Try the following commands in the interactive shell of the container:\n",
    "\n",
    "`$ pwd`  <font color=gray> # print working directory in the container </font >\n",
    "\n",
    "\n",
    "`$ ls`  <font color=gray> # list files in the current directory in the container </font >\n",
    "\n",
    "\n",
    "`$ hostname`  <font color=gray> # display hostname of the container </font >\n",
    "\n",
    "\n",
    "`$ hostname -i`  <font color=gray> # display IP of the container </font >\n",
    "\n",
    "\n",
    "`$ uname -sr`  <font color=gray> # display kernel name and kernel release of the container </font > \n",
    "\n",
    "\n",
    "`$ ps aux`  <font color=gray> # display all processes </font >\n",
    "\n",
    "`$ whoami`  <font color=gray> # display current user </font >\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(235, 235, 224); border:1px solid transparent; border-color: (235, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**Run in a new terminal a second container with the same image**\n",
    "\n",
    "`$ docker container run -it --name cont_ex1_int2 alpine:3.20 ash`\n",
    "\n",
    "and try the following commands:\n",
    "\n",
    "`$ hostname`\n",
    "\n",
    "`$ hostname -i`\n",
    "\n",
    "</div >"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exit from both containers in terminal window."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List active containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List all containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Run a container in detached mode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To start a container in detached mode use `--detach` flag (alternatively `-d`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run \\\n",
    "    -d     \\\n",
    "    --name cont_ex1_detached alpine:3.20 /bin/ash \\\n",
    "    -c 'x=0; fin=150; while [ $x -ne $fin ] ; do echo hello world ${x} ; sleep 1;  x=$((x+1)); done' "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List active containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### See logs of a container"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Logs of a container can be displayed using `docker container logs` command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display logs for **cont_ex1_detached**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container logs cont_ex1_detached"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stop a running container"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A Docker container can be stopped using `docker container stop` command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stop container **cont_ex1_detached**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container stop cont_ex1_detached"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List active containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List all containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Remove a container"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A Docker container can be removed using `docker container rm` command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remove container **cont_ex1_detached**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container rm cont_ex1_detached"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remove multiple containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container rm cont_ex1_int1 cont_ex1_int2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Remove unused containers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All unused/inactive containers can be removed at once using `docker container prune` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container prune --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(255, 230, 230); border:1px solid transparent; border-radius: 6px; border-color: rgb(255, 230, 230); padding:10px; margin: 10px;\">\n",
    "\n",
    "**ATTENTION**\n",
    "\n",
    "Do not run this command if you do not want to delete all your unused containers.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# docker container prune -f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List all containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container ls -a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### List Docker images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List local Docker images:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Remove an image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A docker image can be removed using `docker image rm` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image rm img_ex1:v1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## System Commands"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker system --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display Docker disk usage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker system df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display system info "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Display Docker system-wide information:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker system info"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display system events"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(235, 235, 224); border:1px solid transparent; border-color: (235, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**Run in a terminal**\n",
    "\n",
    "`$ docker system events`\n",
    "\n",
    "</div >"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Restart the detached container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run \\\n",
    "    -d     \\\n",
    "    --name cont_ex1_detached alpine:3.20 /bin/ash \\\n",
    "    -c 'x=0; fin=50; while [ $x -ne $fin ] ; do echo hello world ${x} ; sleep 1;  x=$((x+1)); done' "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stop the container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container stop cont_ex1_detached"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remove the container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container rm cont_ex1_detached cont_ex1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image rm alpine:3.20 hello-world:latest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Summary**\n",
    "\n",
    "**Docker** is a tool to develop, ship and run applications. It creates an isolated environment for the application including its dependencies. \n",
    "    \n",
    "Docker uses a client-server architecture. Docker client is a command line tool to communicate with Docker daemon through a REST API.\n",
    "\n",
    "A **Docker image** is a blueprint from which individual containers are created. It contains the application and all its dependencies that it needs to run correctly. \n",
    "\n",
    "The images are immutable.\n",
    "\n",
    "A **Docker container** in runnable instance created from a Docker image.\n",
    "    \n",
    "Docker client commands are grouped as **Management Commands** on logical objects or CLI plugins:\n",
    "\n",
    "\n",
    "| Logical Object   |            |\n",
    "|------------------|:-----------|    \n",
    "|  `docker image`    |   Manage images |\n",
    "| |  ls |\n",
    "| |  pull |\n",
    "| |  rm |\n",
    "| `docker container` |   Manage containers |\n",
    "| |  ls |\n",
    "| |  ls -a |\n",
    "| |  run |\n",
    "| |  run --name | \n",
    "| |  run -it |\n",
    "| |  logs |\n",
    "| |  stop |\n",
    "| |  rm |\n",
    "| |  prune |\n",
    "|  `docker buildx`   |   Extended build capabilities with BuildKit |\n",
    "| |  build |\n",
    "| |  build -t |\n",
    "|  `docker system`   |   Manage Docker |\n",
    "| |  df |\n",
    "| |  info |\n",
    "| |  events |\n",
    "|  `docker network`  |   Manage networks |\n",
    "|  `docker volume`   |   Manage volumes |\n",
    "\n",
    "Docker client command format:\n",
    "    \n",
    "`docker <object> <command> [options]`\n",
    "\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
