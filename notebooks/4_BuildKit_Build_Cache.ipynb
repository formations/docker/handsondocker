{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Buildkit, Build Cache"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Objective**\n",
    "\n",
    "This notebooks explains how Docker processes instructions in Dockerfile and leverage build cache in order to speed up image builds.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When a Docker image is built from a Dockerfile, Docker BuildKit will cache the results of the build to be reused by subsequent builds. This helps avoid running the same build steps each time you build an image and thus speeds up your build significantly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Dockerfile](../examples/ex4.1/Dockerfile) under **../examples/ex4.1/** has the following content:\n",
    "\n",
    "```Dockerfile\n",
    "ARG BASE_IMAGE=debian:bullseye-slim\n",
    "FROM $BASE_IMAGE\n",
    "\n",
    "LABEL maintainer=\"hande.gozukan@inria.fr\" \\\n",
    "      description=\"A Dockerfile example that uses python script.\"\n",
    "\n",
    "ARG USERNAME\n",
    "ARG UID=1001\n",
    "ARG GID=1001\n",
    "\n",
    "ENV USER=${USERNAME:-appuser}\n",
    "\n",
    "WORKDIR /home/$USER\n",
    "\n",
    "COPY hello.py .\n",
    "\n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "\n",
    "USER $USER\n",
    "\n",
    "ENTRYPOINT [\"python3.9\", \"hello.py\"]\n",
    "\n",
    "CMD [\"stranger\"]\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[hello.py](../examples/ex4.1/hello.py) in the same directory:\n",
    "\n",
    "```python\n",
    "import sys\n",
    "\n",
    "name = sys.argv[1]\n",
    "\n",
    "print(f\"Hello {name} from container!\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build Docker image and examine the outputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_1:v1 ../examples/ex4.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that only 5 instructions in the file created a build step and hence a layer.\n",
    "\n",
    "```\n",
    " => [1/5] FROM docker.io/library/debian:bullseye-slim@sha256:60a596681410  1.5s\n",
    "...\n",
    " => [2/5] WORKDIR /home/appuser                                            0.1s\n",
    " => [3/5] COPY hello.py .                                                  0.0s\n",
    " => [4/5] RUN apt-get update && apt-get install -y     python3.9     pyt  16.0s\n",
    " => [5/5] RUN groupadd appuser --gid 1001 &&     useradd appuser --uid 10  0.3s\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's rebuild another image from the same Dockerfile and compare the outputs with the outputs of the previous build:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_1:v2 ../examples/ex4.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we see that the build took much less time compared to the previous run, and the output is different and shorter.\n",
    "\n",
    "```\n",
    "=> [1/5] FROM docker.io/library/debian:bullseye-slim@sha256:60a596681410  0.0s\n",
    " ...\n",
    " => CACHED [2/5] WORKDIR /home/appuser                                     0.0s\n",
    " => CACHED [3/5] COPY hello.py .                                           0.0s\n",
    " => CACHED [4/5] RUN apt-get update && apt-get install -y     python3.9    0.0s\n",
    " => CACHED [5/5] RUN groupadd appuser --gid 1001 &&     useradd appuser -  0.0s\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While building the image, the instructions in the Dockerfile are executed in top-down order. For each instruction BuildKit verifies if there already exists a layer that corresponds to that specific instruction in the build cache. If it exists, BuildKit uses that layer from the cache. \n",
    "\n",
    "For the above build, we have made no changes to the Dockerfile. We observe `---> CACHED` lines meaning that BuildKit is using the layer from cache for that instruction.\n",
    "\n",
    "We can easily verify that the image IDs and sizes for images img_ex4_1:v1 and img_ex4_1:v2 are exactly the same. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image ls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will build a new image with the [Dockerfile](../examples/ex4.2/Dockerfile) under **../examples/ex4.2/**. The only difference is the line where we install the Python package [Faker](https://pypi.org/project/Faker/) `RUN pip3 install faker`.:\n",
    "\n",
    "```Dockerfile\n",
    "ARG BASE_IMAGE=debian:bullseye-slim\n",
    "FROM $BASE_IMAGE\n",
    "\n",
    "LABEL maintainer=\"hande.gozukan@inria.fr\" \\\n",
    "      description=\"A Dockerfile example that uses python script.\"\n",
    "\n",
    "ARG USERNAME\n",
    "ARG UID=1001\n",
    "ARG GID=1001\n",
    "\n",
    "ENV USER=${USERNAME:-appuser}\n",
    "\n",
    "WORKDIR /home/$USER\n",
    "\n",
    "COPY hello.py .\n",
    "\n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "\n",
    "RUN pip3 install faker\n",
    "\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "\n",
    "USER $USER\n",
    "\n",
    "ENTRYPOINT [\"python3.9\", \"hello.py\"]\n",
    "\n",
    "CMD [\"stranger\"]\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We copy `hello.py` file from [ex4.1](../examples/ex4.1/hello.py) under ../examples/ex4.2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cp ../examples/ex4.1/hello.py ../examples/ex4.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build Docker image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_2:v1 ../examples/ex4.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at the build output.\n",
    "\n",
    "```\n",
    " => [1/6] FROM docker.io/library/debian:bullseye-slim@sha256:60a596681410  0.0s\n",
    " ...\n",
    " => CACHED [2/6] WORKDIR /home/appuser                                     0.0s\n",
    " => CACHED [3/6] COPY hello.py .                                           0.0s\n",
    " => CACHED [4/6] RUN apt-get update && apt-get install -y     python3.9    0.0s\n",
    " => [5/6] RUN pip3 install faker                                           2.4s\n",
    " => [6/6] RUN groupadd appuser --gid 1001 &&     useradd appuser --uid 10  0.4s\n",
    "\n",
    "```\n",
    "\n",
    "Here we see that BuildKit uses the layers from build cache until \n",
    "\n",
    "`Step 4/6 : RUN apt-get update && apt-get install -y     python3.9`. \n",
    "\n",
    "This step corresponds to the instruction that we added to the Dockerfile. From this step on Docker no more checks for existing layers in build cache and creates new layers for the steps 5 and 6."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will modify the contents of [hello.py](../examples/ex4.2/hello.py) under **../examples/ex4.2** as follows:\n",
    "\n",
    "```python\n",
    "import sys\n",
    "\n",
    "from faker import Faker\n",
    "\n",
    "name = sys.argv[1]\n",
    "\n",
    "fake = Faker('fr_FR')\n",
    "\n",
    "fname = fake.name()\n",
    "faddr = fake.address()\n",
    "\n",
    "print(f\"\"\"Hello {name} from container! \n",
    "\n",
    "Here is your fake name: \n",
    "{fname} \n",
    "\n",
    "and fake address: \n",
    "{faddr}\"\"\")\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build Docker image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_2:v2 ../examples/ex4.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have not changed the contents of the Dockerfile, however we have changed the contents of *hello.py* that is copied inside the image. Starting from \n",
    "\n",
    "`[3/6] COPY hello.py .` \n",
    "\n",
    "BuildKit stopped checking the build cache and created a new layer for each step."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px;background-color: rgb(224, 235, 235); padding:10px;\">\n",
    "\n",
    "To match a layer from the cache, BuildKit:\n",
    "\n",
    "* Starts with a base image, and checks if there is a layer that is derived from that base image with an instruction exactly the same as the next instruction in the Dockerfile. If it does not exist, the cache is invalidated and for each following instruction Docker creates a new layer.\n",
    "\n",
    "* If the instruction is exactly the same, for all instructions but `COPY` and `ADD` instructions, Docker reuses image layers from build cache.\n",
    "\n",
    "* For `COPY` and `ADD` instructions, a checksum is calculated for the files copied inside the container. This checksum is calculated using the contents and metadata of the files, except last-modified and last-accessed times.\n",
    "\n",
    "* Apart from files copied to image using `COPY` and `ADD` instructions Docker makes no control on the files inside the image. eg. `RUN apt-get update`\n",
    "\n",
    "* Only the commands that modify the filesystem create a layer, others only change metadata of the image.\n",
    "\n",
    "<font color=gray>For more info on **Build Cache**, see [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache).</font>\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"border:rgb(0,255,0) 1.0px; background-color: rgb(224, 235, 224); border:1px solid transparent; border-radius: 6px; border-color: rgb(224, 235, 224); padding:10px; margin: 10px;\">\n",
    "    \n",
    "**NOTE**\n",
    "    \n",
    "If you do not want to use cache while building the image, you can use `--no-cache` flag.\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run Docker container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --rm img_ex4_2:v2 Peppa"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image layers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's check the image layers for the base image **debian:bullseye-slim**, **img_ex4_1:v1**, **img_ex4_2:v1** and **img_ex4_2:v2** in more detail. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image pull debian:bullseye-slim"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image inspect -f '{{join .RootFS.Layers \"\\n\"}}' debian:bullseye-slim"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have the following layers for image **img_ex4_1:v1**:\n",
    "```\n",
    " => [1/5] FROM docker.io/library/debian:bullseye-slim@sha256:60a596681410  1.5s\n",
    "...\n",
    " => [2/5] WORKDIR /home/appuser                                            0.1s\n",
    " => [3/5] COPY hello.py .                                                  0.0s\n",
    " => [4/5] RUN apt-get update && apt-get install -y     python3.9     pyt  16.0s\n",
    " => [5/5] RUN groupadd appuser --gid 1001 &&     useradd appuser --uid 10  0.3s\n",
    " \n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image inspect -f '{{join .RootFS.Layers \"\\n\"}}' img_ex4_1:v1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we have the following layers for image **** and **** with the difference in contents of `hello.py` file.\n",
    "\n",
    "```\n",
    " => [1/6] FROM docker.io/library/debian:bullseye-slim@sha256:60a596681410  0.0s\n",
    " ...\n",
    " => CACHED [2/6] WORKDIR /home/appuser                                     0.0s\n",
    " => CACHED [3/6] COPY hello.py .                                           0.0s\n",
    " => CACHED [4/6] RUN apt-get update && apt-get install -y     python3.9    0.0s\n",
    " => [5/6] RUN pip3 install faker                                           2.4s\n",
    " => [6/6] RUN groupadd appuser --gid 1001 &&     useradd appuser --uid 10  0.4s\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "docker image inspect -f '{{join .RootFS.Layers \"\\n\"}}' img_ex4_2:v1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image inspect -f '{{join .RootFS.Layers \"\\n\"}}' img_ex4_2:v2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that two images, **img_ex4_2:v1** and **img_ex4_2:v2** have common layers until \n",
    "\n",
    "`WORKDIR /home/appuser` which has digest `a121bd758dfb82004770cf99f239c4f12048983d92f8953c58ec75c01b3a96cc`\n",
    "\n",
    "and then the layer digests are different."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Efficient use of build cache"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Efficient use of build cache saves a lot of time for subsequent builds. \n",
    "\n",
    "In order to minimize invalidation of cache:\n",
    "\n",
    "* Only copy the necessary files in the next step\n",
    "* Put instructions that are more likely to change later in the file "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the Dockerfile under **../examples/ex4.2/**, we can move \n",
    "\n",
    "```Dockerfile\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "```\n",
    "\n",
    "and \n",
    "\n",
    "```Dockerfile\n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "```\n",
    "\n",
    "instructions above, as they are less likely to change. \n",
    "\n",
    "We assume that `hello.py` file is more subject to change."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Dockerfile](../examples/ex4.3/Dockerfile) under **../examples/ex4.3/** becomes:\n",
    "\n",
    "```Dockerfile\n",
    "ARG BASE_IMAGE=debian:bullseye-slim\n",
    "FROM $BASE_IMAGE\n",
    "\n",
    "LABEL maintainer=\"hande.gozukan@inria.fr\" \\\n",
    "      description=\"A Dockerfile example that uses python script.\"\n",
    "\n",
    "ARG USERNAME\n",
    "ARG UID=1001\n",
    "ARG GID=1001\n",
    "\n",
    "ENV USER ${USERNAME:-appuser}\n",
    "\n",
    "RUN groupadd $USER --gid $GID && \\\n",
    "    useradd $USER --uid $UID \\\n",
    "    --gid $GID \\\n",
    "    --shell /bin/bash \\\n",
    "    --create-home \n",
    "    \n",
    "RUN apt-get update && apt-get install -y \\\n",
    "    python3.9 \\\n",
    "    python3-pip\n",
    "\n",
    "WORKDIR /home/$USER\n",
    "\n",
    "RUN pip3 install faker\n",
    "\n",
    "COPY hello.py .\n",
    "\n",
    "USER $USER\n",
    "\n",
    "ENTRYPOINT [\"python3.9\", \"hello.py\"]\n",
    "\n",
    "CMD [\"stranger\"]\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We copy `hello.py` file from [ex4.2](../examples/ex4.2/hello.py) under ../examples/ex4.3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cp ../examples/ex4.2/hello.py ../examples/ex4.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "docker image build -t img_ex4_3:v1 ../examples/ex4.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run container:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker container run --rm img_ex4_3:v1 Peppa"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docker image inspect -f '{{join .RootFS.Layers \"\\n\"}}' img_ex4_3:v1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will continue working on this Dockerfile in the next notebook and take advantage of build cache to create a Docker image which generates random data and persists it in a CSV file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\" border:rgb(0,255,0) 1.0px; background-color: rgb(242, 242, 242); border:1px solid transparent; border-color: rgb(242, 242, 242); padding:10px; margin: 10px;\">\n",
    "\n",
    "**Summary**\n",
    "\n",
    "While building the image, BuildKit executes each instruction in the Dockerfile in top-down order. For each instruction BuildKit verifies if there already exists a layer that corresponds to that specific instruction in the build cache. If it exists, BuildKit uses that layer from the cache. \n",
    "\n",
    "To match a layer from the cache, BuildKit:\n",
    "\n",
    "* Starts with a base image, and checks if there is a layer that is derived from that base image with an instruction exactly the same as the next instruction in the Dockerfile. If it does not exist, the cache is invalidated and for each following instruction Docker creates a new layer.\n",
    "\n",
    "* If the instruction is exactly the same, for all instructions but `COPY` and `ADD` instructions, Docker reuses image layers from build cache.\n",
    "\n",
    "* For `COPY` and `ADD` instructions, a checksum is calculated for the files copied inside the container. This checksum is calculated using the contents and metadata of the files, except last-modified and last-accessed times.\n",
    "\n",
    "* Apart from files copied to image using `COPY` and `ADD` instructions Docker makes no control on the files inside the image. eg. `RUN apt-get update`\n",
    "\n",
    "* Only the commands that modify the filesystem create a layer, others only change metadata of the image.\n",
    "\n",
    "\n",
    "Efficient use of build cache speeds up image building significantly.\n",
    "\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
