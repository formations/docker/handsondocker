import sys
import os
import psycopg2

from faker import Faker
from faker.providers import bank, job, phone_number

count = 10

dbname = os.environ['DBNAME']
dbhost = os.environ['DBHOST']
dbport = os.environ['DBPORT']
dbuser = os.environ['DBUSER']
dbpasswd = os.environ['DBPASSWD']

print(f"""Hello from container!
I am generating {count} fake data for you.""")

conn = psycopg2.connect(host=dbhost, 
                        port = dbport, 
                        database=dbname, 
                        user=dbuser, 
                        password=dbpasswd)

# Create a cursor object
cur = conn.cursor()

cur.execute("CREATE TABLE IF NOT EXISTS fakers (id SERIAL PRIMARY KEY, name VARCHAR, iban VARCHAR, job VARCHAR, phone VARCHAR)")


faker = Faker('fr_FR')
faker.add_provider(bank)
faker.add_provider(job)
faker.add_provider(phone_number)

entry = ""

for i in range(count):
    fname = faker.name()
    fiban = faker.iban()
    fjob = faker.job()
    fphone = faker.phone_number()

    cur.execute("INSERT INTO fakers (name, iban, job, phone) VALUES(%s, %s, %s, %s)",
                   (fname, fiban, fjob, fphone)
                  )

conn.commit() 
cur.close()
conn.close()

print(f"Data generated and written to {dbname} db!")